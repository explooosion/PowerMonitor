﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data.OleDb;

public partial class Main : System.Web.UI.Page
{
    public OleDbConnection db = null;
    public OleDbCommand cmd = null;
    public LoginSession ls;

    protected void Page_Load(object sender, EventArgs e)
    {
        ls = (LoginSession)Session["LoginSession"];
        if (ls != null)
        {
            if (ls.IsLogin == true)
            {
                linkName.Text = ls.UserName;
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }


    protected void ddlUserName_SelectedIndexChanged(object sender, EventArgs e)
    {
        string uname = ddlUserName.SelectedValue;
        if (uname.Length != 0)
        {
            LoadUserData(ddlUserName.SelectedValue);
        }
    }

    private void LoadUserData(string username)
    {
        db = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|data.accdb");
        db.Open();

        string sql = "SELECT * FROM UserList WHERE username = ? ";
        cmd = new OleDbCommand(sql, db);
        cmd.Parameters.Clear();
        cmd.Parameters.Add(new OleDbParameter("?", username));

        OleDbDataReader reader = cmd.ExecuteReader();

        while (reader.Read())
        {
            txtUserPassword.Text = reader.GetString(2);         // 密碼
            txtTimeout.Text = reader.GetValue(4).ToString();    // 間隔
            GetPower(reader.GetValue(5).ToString());            // 權限
            GetDisplay(reader.GetString(6));                    // 檢視
        }

        reader.Close();
        db.Close();
        db.Dispose();
    }


    private void GetPower(string power)
    {
        string sql = "SELECT power FROM PowerList WHERE id = ? ";
        cmd = new OleDbCommand(sql, db);
        cmd.Parameters.Clear();
        cmd.Parameters.Add(new OleDbParameter("?", power));

        OleDbDataReader reader = cmd.ExecuteReader();
        while (reader.Read())
        {
            ddlPower.SelectedIndex = ddlPower.Items.IndexOf(ddlPower.Items.FindByValue(reader.GetString(0)));
        }
        reader.Close();
    }


    private void GetDisplay(string display)
    {
        chkDisplay.ClearSelection();
        string[] displayArr = display.Split(',');

        foreach (string _displayArr in displayArr)
        {
            string sql = "SELECT title FROM DisplayList WHERE id = ? ";
            cmd = new OleDbCommand(sql, db);
            cmd.Parameters.Clear();
            cmd.Parameters.Add(new OleDbParameter("?", _displayArr));

            OleDbDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                chkDisplay.Items.FindByValue(reader.GetString(0)).Selected = true;
            }
            reader.Close();
        }
    }


    protected void btnPanelSubmit_Click(object sender, EventArgs e)
    {
        Boolean EditMode = txtUserName.Text == "" ? true : false;

        db = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|data.accdb");
        db.Open();
        string sql = string.Empty;
        OleDbDataReader reader;

        //Get UserName
        string username = EditMode ? ddlUserName.SelectedValue : txtUserName.Text;

        //Get UserPassword
        string passwd = txtUserPassword.Text;

        //Get Power
        int power = 2;
        sql = "SELECT id FROM PowerList WHERE power = ? ";
        cmd = new OleDbCommand(sql, db);
        cmd.Parameters.Clear();
        cmd.Parameters.Add(new OleDbParameter("?", ddlPower.SelectedValue));

        reader = cmd.ExecuteReader();
        while (reader.Read())
        {
            power = Convert.ToInt32(reader.GetValue(0));
        }
        reader.Close();


        //Get TimeOut
        int time = Convert.ToInt32(txtTimeout.Text);

        //Get Display
        string display = string.Empty;
        List<ListItem> displayList = chkDisplay.Items.Cast<ListItem>().Where(li => li.Selected).ToList();
        foreach (ListItem li in displayList)
        {
            sql = "SELECT id FROM DisplayList WHERE title = ? ";
            cmd = new OleDbCommand(sql, db);
            cmd.Parameters.Clear();
            cmd.Parameters.Add(new OleDbParameter("?", li.Value));

            reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                display += reader.GetValue(0).ToString() + ",";
            }
            reader.Close();
        }
        display = display.Substring(0, display.Length - 1);

        //Update Data
        string sql_update = "UPDATE UserList SET passwd=@passwd, status=@status, timeout=@timeout, power=@power, display=@display WHERE username=@username";
        string sql_insert = "INSERT INTO UserList (username,passwd,status,timeout,power,display) VALUES (@username,@passwd,@status,@time,@power,@display)";


        sql = EditMode ? sql_update : sql_insert;

        cmd = new OleDbCommand(sql, db);
        cmd.Parameters.Clear();
        if (sql == sql_update)
        {
            cmd.Parameters.Add(new OleDbParameter("@passwd", passwd));
            cmd.Parameters.Add(new OleDbParameter("@status", "未連線"));
            cmd.Parameters.Add(new OleDbParameter("@timeout", time));
            cmd.Parameters.Add(new OleDbParameter("@power", power));
            cmd.Parameters.Add(new OleDbParameter("@display", display));
            cmd.Parameters.Add(new OleDbParameter("@username", username));
        }
        else if (sql == sql_insert)
        {
            cmd.Parameters.Add(new OleDbParameter("@username", username));
            cmd.Parameters.Add(new OleDbParameter("@passwd", passwd));
            cmd.Parameters.Add(new OleDbParameter("@status", "未連線"));
            cmd.Parameters.Add(new OleDbParameter("@timeout", time));
            cmd.Parameters.Add(new OleDbParameter("@power", power));
            cmd.Parameters.Add(new OleDbParameter("@display", display));
        }

        try
        {
            cmd.ExecuteNonQuery();
        }
        catch (Exception ex)
        {
            System.Diagnostics.Debug.WriteLine(ex);
            //Response.Redirect("Login.aspx");
        }
        finally
        {
            db.Close();
            db.Dispose();
            Response.Redirect(Request.RawUrl);
        }
    }


    protected void btnDelSubmit_Click(object sender, EventArgs e)
    {
        db = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|data.accdb");
        db.Open();
        string sql = string.Empty;

        sql = "Delete FROM UserList WHERE username = ? ";
        cmd = new OleDbCommand(sql, db);
        cmd.Parameters.Clear();
        cmd.Parameters.Add(new OleDbParameter("?", ddlDelUserName.SelectedValue));

        try
        {
            cmd.ExecuteNonQuery();
        }
        catch (Exception ex)
        {
            System.Diagnostics.Debug.WriteLine(ex);
        }
        finally
        {
            db.Close();
            db.Dispose();
            Response.Redirect(Request.RawUrl);
        }
    }

    /// <summary>
    /// 事件_登出
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void linkLogout_Click(object sender, EventArgs e)
    {
        // Clear Cookie & Session
        HttpCookie CookieUser = new HttpCookie("user");
        CookieUser.Expires = DateTime.Now.AddDays(-1d);
        Response.Cookies.Add(CookieUser);

        Session.Clear();
        Session.Abandon();
        Response.Redirect("Login.aspx");
    }


    /// <summary>
    /// 事件_資料載入時調整列表
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Style.Add("line-height", "34px");

            string display = e.Row.Cells[3].Text;
            string display_new = string.Empty;
            if (display.Length > 0)
            {
                string[] displayArr = display.Split(',');
                db = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|data.accdb");
                db.Open();

                foreach (string darr in displayArr)
                {
                    string sql = "SELECT title FROM DisplayList WHERE id = ? ";
                    cmd = new OleDbCommand(sql, db);
                    cmd.Parameters.Clear();
                    cmd.Parameters.Add(new OleDbParameter("?", darr));
                    OleDbDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        display_new += reader.GetValue(0).ToString() + "<br />";
                    }
                    reader.Close();
                }

                e.Row.Cells[3].Text = display_new;
                db.Close();
                db.Dispose();
            }
        }
    }
}