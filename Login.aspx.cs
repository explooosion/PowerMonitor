﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Login : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        /// 是否記住帳密

        System.Diagnostics.Debug.WriteLine(Request.Cookies["user"]);
        if (Request.Cookies["user"] != null)
        {
            string username = Request.Cookies["user"]["username"];
            string password = Request.Cookies["user"]["passwd"];
            System.Diagnostics.Debug.WriteLine("ID:" + Request.Cookies["user"]["username"]);
            System.Diagnostics.Debug.WriteLine("PWD:" + Request.Cookies["user"]["passwd"]);

            LoginSessionEvent(new LoginSession(username, password));

        }
    }


    protected void btnLogin_Click(object sender, EventArgs e)
    {
        string username = txtUserName.Text;
        string password = txtPassword.Text;

        LoginSessionEvent(new LoginSession(username, password));

    }



    private void LoginSessionEvent(LoginSession ls)
    {
        if (ls.IsLogin)
        {

            // 自動記住帳密
            HttpCookie myCookie = new HttpCookie("user");
            myCookie["username"] = ls.UserName;
            myCookie["passwd"] = ls.Password;
            myCookie.Expires = DateTime.Now.AddDays(1d);
            Response.Cookies.Add(myCookie);

            Session.Add("LoginSession", ls);

            if (ls.Power == 2)
            {
                if (ls.HasIEK)
                {
                    Response.Redirect("iek.aspx");
                }
                else
                {
                    Response.Redirect("powerChannel.aspx");
                }
            }

            Response.Redirect("Main.aspx");
        }
        else
        {
            // Clear Cookie & Session
            HttpCookie CookieUser = new HttpCookie("user");
            CookieUser.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(CookieUser);

            Session.Clear();
            Session.Abandon();
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('帳號或密碼錯誤!')", true);
        }
    }
}