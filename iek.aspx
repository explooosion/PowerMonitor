﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="iek.aspx.cs" Inherits="iek" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentHead" runat="Server">

    <header class="page-header" id="page-header">
        <div class="container">
            <div class="main-header">
                <!-- Branding Name -->
                <div class="navbar-header">
                    <div class="branding wide">
                        <h1 class="header-logo">IEK太陽光電發電系統</h1>
                        <!-- <div class="completion-date">竣工日 2016.07.31</div> -->
                    </div>
                    <!--/branding-->

                    <!--current Time-->
                    <!-- <div class="current-date-time" id="showDate">
                <div> <span class="date">2016<small>/</small>09<small>/</small>07</span> <span class="time">Am 08:00:00</span></div>
              </div> -->
                    <div class="current-date-time has-setting">
                        <div class="date-time" id="showDate">
                            <span class="date">2016<small>/</small>09<small>/</small>07</span>
                            <span class="time">Am 08:00:00</span>
                        </div>
                        <div class="setting-wrap">
                            <a href="javascript: _logout();" class="btn-setting">
                                <span class="iek-icon icon-10"></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="contentMain" runat="Server">

    <div class="main-content">
        <div class="flex-container">
            <div class="flex-column-1 ">
                <div class="iek-control-panel">
                    <div class="wrapper">
                        <div class="row gap-none">

                            <div class="col-xs-6">
                                <artice class="data-item">
                      <!-- 設置容量 -->
                      <div class="iek-icon icon-1"></div>
                      <div class="iek-data">
                        <div class="data-number">
                          <span id="setAmount"></span>
                          <small class="unit">wp</small>
                        </div>
                        <div class="progress">
                          <div id="setAmount-percent" class="progress-bar bar-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                          </div>
                        </div>
                        <div class="data-name">
                          設置容量
                        </div>
                      </div>
                    </artice>
                            </div>

                            <div class="col-xs-6">
                                <artice class="data-item">
                      <!-- 即時發電量 -->
                      <div class="iek-icon icon-2"></div>
                      <div class="iek-data">
                        <div class="data-number">
                          <span id="instantPower"></span>
                          <small class="unit">wp</small>
                        </div>
                        <div class="progress">
                          <div id="instantPower-percent" class="progress-bar bar-primary" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                          </div>
                        </div>
                        <div class="data-name">
                          即時發電量
                        </div>
                      </div>
                    </artice>
                            </div>

                            <div class="col-xs-6">
                                <artice class="data-item">
                      <!-- 佔10館即時用電比例 -->
                      <div class="iek-icon icon-13"></div>
                      <div class="iek-data">
                        <div class="data-number">
                          <span id="buliding"></span>
                          <small class="unit">%</small>
                        </div>
                        <div class="progress">
                          <div id="buliding-percent" class="progress-bar bar-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                          </div>
                        </div>
                        <div class="data-name">
                          佔 10 館即時用電比例
                        </div>
                      </div>
                    </artice>
                            </div>

                            <div class="col-xs-6">
                                <artice class="data-item">
                      <!-- 佔 10 館月累積用電比例 -->
                      <div class="iek-icon icon-14"></div>
                      <div class="iek-data">
                        <div class="data-number">
                          <span id="monthly"></span>
                          <small class="unit">%</small>
                        </div>
                        <div class="progress">
                          <div id="monthly-percent" class="progress-bar bar-primary" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                          </div>
                        </div>
                        <div class="data-name">
                          佔 10 館月累積用電比例
                        </div>
                      </div>
                    </artice>
                            </div>

                            <div class="col-xs-6">
                                <artice class="data-item">
                      <!-- 累計總發電量 -->
                      <div class="iek-icon icon-3"></div>
                      <div class="iek-data">
                        <div class="data-number">
                          <span id="totalPower"></span>
                          <small class="unit">KWh</small>
                        </div>
                        <div class="progress">
                          <div id="totalPower-percent" class="progress-bar bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                          </div>
                        </div>
                        <div class="data-name">
                          累計總發電量
                        </div>
                      </div>
                                </article>
                 
                            </div>

                            <div class="col-xs-6">
                                <artice class="data-item">
                      <!-- 減少CO2量 -->
                      <div class="iek-icon icon-4"></div>
                      <div class="iek-data">
                        <div class="data-number">
                          <span id="reduceCo2"></span>
                          <small class="unit">噸</small>
                        </div>
                        <div class="progress">
                          <div id="reduceCo2-percent" class="progress-bar bar-info" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                          </div>
                        </div>
                        <div class="data-name">
                          減少CO2量
                        </div>
                      </div>
                                </article>
                 
                            </div>

                            <div class="col-xs-6">
                                <artice class="data-item">

                      <!-- 累積發電時數 -->
                      <div class="iek-icon icon-5"></div>
                      <div class="iek-data">
                        <div class="data-number">
                          <span id="totalHours"></span>
                          <small class="unit">日</small>
                        </div>
                        <div class="progress">
                          <div id="totalHours-percent" class="progress-bar bar-danger" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                          </div>
                        </div>
                        <div class="data-name">
                          累積發電時數
                        </div>
                      </div>
                                </article>
                 
                            </div>

                            <div class="col-xs-6">
                                <artice class="data-item">
                      <!-- 提供照明電量 -->
                      <div class="iek-icon icon-6"></div>
                      <div class="iek-data">
                        <div class="data-number">
                          <span id="illuminePower"></span>
                        </div>
                        <div class="progress">
                          <div id="illuminePower-percent" class="progress-bar bar-purple" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                          </div>
                        </div>
                        <div class="data-name">
                          提供照明電量
                        </div>
                      </div>
                                </article>
                 
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="flex-column-1 ">
                <div class="panel-grand-total auto-height">
                    <div id="firstChart" style="height: 100%"></div>
                </div>
            </div>
        </div>
        <div class="flex-container">
            <div class="flex-column-1">
                <div class="iek-monthly-electricity">
                    <div id="secondChart" style="height: 100%"></div>
                </div>

            </div>
            <div class="flex-column-1">
                <div class="iek-yearly-electricity">
                    <div id="thirdChart" style="height: 100%"></div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="contentScript" runat="Server">
    <script src="js/iek-main.js"></script>
    <script src="js/showTime.js"></script>
</asp:Content>
