﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Main.aspx.cs" Inherits="Main" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>電力資訊廣播系統</title>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="js/common.js"></script>
</head>
<body>
    <form id="form1" runat="server">

        <!-- Header -->
        <div class="head">
            <a href="Main.aspx">
                <h1>電力資訊廣播系統</h1>
            </a>
            <div class="panel-account"></div>
        </div>

        <div class="container">

            <!-- Controller -->
            <nav class="panel-control">
                <ul class="control-group">
                    <li><a class="control" href="javascript: _addUser();">新增</a></li>
                    <li><a class="control" href="javascript: _editUser();">編輯</a></li>
                    <li><a class="control" href="javascript: _deleteUser();">刪除</a></li>
                </ul>
                <ul class="control-group control-group-right">
                    <li>
                        <asp:HyperLink runat="server" ID="linkName" CssClass="control"></asp:HyperLink></li>
                    <li>
                        <asp:LinkButton runat="server" ID="linkLogout" CssClass="control" Text="登出" OnClientClick="return confirm('確定要登出嗎?');" OnClick="linkLogout_Click"></asp:LinkButton></li>
                </ul>
            </nav>

            <!-- User List -->
            <div class="panel-list">
                <asp:SqlDataSource
                    ID="sqlUserList"
                    runat="server"
                    ConnectionString="<%$ ConnectionStrings:CustomerDataConnectionString %>"
                    ProviderName="<%$ ConnectionStrings:CustomerDataConnectionString.ProviderName %>"
                    SelectCommand="SELECT UserList.username, UserList.timeout, PowerList.power, UserList.display FROM UserList LEFT JOIN PowerList ON UserList.power = PowerList.id" />
                <asp:GridView ID="gridList" runat="server" DataSourceID="sqlUserList" CssClass="table-list" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" GridLines="None" OnRowDataBound="gridList_RowDataBound">
                    <Columns>
                        <asp:BoundField DataField="username" HeaderText="帳號" />
                        <asp:BoundField DataField="timeout" HeaderText="間隔時間(秒)" />
                        <asp:BoundField DataField="power" HeaderText="權限" />
                        <asp:BoundField DataField="display" HeaderText="顯示設定" />
                    </Columns>
                </asp:GridView>
            </div>
        </div>

        <!-- Panel Dialog -->
        <div id="dialogEdit" class="panel">
            <div class="panel-form">
                <div class="row">
                    <label>帳號名稱：</label>
                    <asp:SqlDataSource
                        ID="sqlUserName"
                        runat="server"
                        ConnectionString="<%$ ConnectionStrings:CustomerDataConnectionString %>"
                        ProviderName="<%$ ConnectionStrings:CustomerDataConnectionString.ProviderName %>"
                        SelectCommand="SELECT username FROM UserList" />
                    <asp:DropDownList runat="server" ID="ddlUserName"
                        DataSourceID="sqlUserName" DataTextField="username" DataValueField="username" BindField='username'
                        OnSelectedIndexChanged="ddlUserName_SelectedIndexChanged"
                        AppendDataBoundItems="true"
                        AutoPostBack="true" CssClass="dropdown right-align" ClientIDMode="Static">
                        <asp:ListItem Text="請選擇帳號" Value=""></asp:ListItem>
                    </asp:DropDownList>
                    <asp:TextBox runat="server" ID="txtUserName" CssClass="text text-lg" ClientIDMode="Static"></asp:TextBox>
                </div>

                <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="UpdatePanel1">
                    <ContentTemplate>
                        <asp:ScriptManager ID="ScriptManager1" runat="server">
                        </asp:ScriptManager>
                        <div class="row">
                            <label>密碼：</label>
                            <asp:TextBox runat="server" ID="txtUserPassword" CssClass="text text-lg right-align"></asp:TextBox>
                        </div>
                        <div class="row">
                            <label>權限：</label>
                            <asp:SqlDataSource
                                ID="sqlPower"
                                runat="server"
                                ConnectionString="<%$ ConnectionStrings:CustomerDataConnectionString %>"
                                ProviderName="<%$ ConnectionStrings:CustomerDataConnectionString.ProviderName %>"
                                SelectCommand="SELECT power FROM PowerList" />
                            <asp:DropDownList runat="server" ID="ddlPower"
                                DataSourceID="sqlPower" DataTextField="power" DataValueField="power" BindField='power'
                                AppendDataBoundItems="true" CssClass="dropdown right-align">
                                <asp:ListItem Text="請選擇權限" Value=""></asp:ListItem>
                            </asp:DropDownList>
                        </div>

                        <div class="row">
                            <label>間隔(秒)：</label>
                            <asp:TextBox runat="server" ID="txtTimeout" CssClass="text text-sm right-align"></asp:TextBox>
                        </div>
                        <div class="row">
                            <label>顯示設定：</label>
                            <div class="check-group">
                                <asp:SqlDataSource
                                    ID="sqlView"
                                    runat="server"
                                    ConnectionString="<%$ ConnectionStrings:CustomerDataConnectionString %>"
                                    ProviderName="<%$ ConnectionStrings:CustomerDataConnectionString.ProviderName %>"
                                    SelectCommand="SELECT title FROM DisplayList ORDER BY id" />
                                <asp:CheckBoxList runat="server" ID="chkDisplay" DataSourceID="sqlView" DataTextField="title" DataValueField="title" BindField='title' CssClass="checklist"></asp:CheckBoxList>
                            </div>
                        </div>
                        <div class="row">
                            <asp:Button runat="server" ID="btnPanelSubmit" CssClass="ui-button button" Text="確定" OnClientClick="return confirm('確定儲存?');" OnClick="btnPanelSubmit_Click" />
                            <asp:Button runat="server" ID="btnPanelCancel" CssClass="ui-button button" Text="取消" OnClientClick="_closeEdit();" />
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ddlUserName" EventName="SelectedIndexChanged" />
                    </Triggers>
                </asp:UpdatePanel>

            </div>


            <div id="dialogDelete" class="panel">
                <div class="panel-form">
                    <div class="row">
                        <label>帳號名稱：</label>
                        <asp:SqlDataSource
                            ID="sqlDelUserName"
                            runat="server"
                            ConnectionString="<%$ ConnectionStrings:CustomerDataConnectionString %>"
                            ProviderName="<%$ ConnectionStrings:CustomerDataConnectionString.ProviderName %>"
                            SelectCommand="SELECT username FROM UserList" />
                        <asp:DropDownList runat="server" ID="ddlDelUserName"
                            DataSourceID="sqlUserName" DataTextField="username" DataValueField="username" BindField='username'
                            AppendDataBoundItems="true" CssClass="dropdown right-align">
                            <asp:ListItem Text="請選擇帳號" Value=""></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="row">
                        <asp:Button runat="server" ID="btnDelSubmit" CssClass="ui-button button" Text="確定" OnClientClick="return confirm('確定刪除?');" OnClick="btnDelSubmit_Click" />
                        <asp:Button runat="server" ID="btnDelCancel" CssClass="ui-button button" Text="取消" OnClientClick="_closeEdit();" />
                    </div>
                </div>
            </div>

        </div>
    </form>
</body>
</html>
