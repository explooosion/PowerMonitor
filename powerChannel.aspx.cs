﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class powerChannel : System.Web.UI.Page
{
    LoginSession ls = null;

    protected void Page_Load(object sender, EventArgs e)
    {

        ls = (LoginSession)Session["LoginSession"];

        if (IsPostBack == true)
        {
            DataRefresh drs = new DataRefresh();
            drs.UpdateSession();
            Response.Redirect(drs.ChangePage());
        }

    }

 
}