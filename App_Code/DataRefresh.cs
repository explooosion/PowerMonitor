﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// DataRefresh 的摘要描述
/// </summary>
public class DataRefresh
{
    public DataRefresh()
    {
        //
        // TODO: 在這裡新增建構函式邏輯
        //
    }


    private string PageLoad = string.Empty;

    public void UpdateSession()
    {

        // 更新 - 館-識別資料
        string[] DisplayNumArray = HttpContext.Current.Session["DisplayNumArray"].ToString().Split(',');
        string[] DisplayTitleArray = HttpContext.Current.Session["DIsplayTitleArray"].ToString().Split(',');
        string[] CustomNumArray = HttpContext.Current.Session["CustomNumArray"].ToString().Split(',');
        bool HasIEK = Convert.ToBoolean(HttpContext.Current.Session["HasIEK"].ToString());

        int Index = Array.IndexOf(DisplayTitleArray, (string)HttpContext.Current.Session["DisplayTitle"]);
        if (Index == DisplayTitleArray.Length - 1)
        {
            // 如已經最後一筆,則重頭取
            HttpContext.Current.Session["DisplayNum"] = DisplayNumArray[0];
            HttpContext.Current.Session["DisplayTitle"] = DisplayTitleArray[0];

            if (CustomNumArray[0] != "")
            {
                this.PageLoad = "Bulletin.aspx";
            }
            else if (HasIEK)
            {
                this.PageLoad = "iek.aspx";
            }
            else
            {
                this.PageLoad = "powerChannel.aspx";
            }
        }
        else
        {

            // 不是最後一筆則執行下方

            Index++;
            HttpContext.Current.Session["DisplayNum"] = DisplayNumArray[Index];
            HttpContext.Current.Session["DisplayTitle"] = DisplayTitleArray[Index];

            this.PageLoad = "powerChannel.aspx";
            /*if (CustomNumArray[0] != "")
            {
                // 如自訂,則跳至自訂畫面
                this.PageLoad = "Bulletin.aspx";
            }
            else
            {
                this.PageLoad = "powerChannel.aspx";
            }
            */

        }

        System.Diagnostics.Debug.WriteLine(DisplayNumArray[Index]);
        System.Diagnostics.Debug.WriteLine(DisplayTitleArray[Index]);

    }

    public string ChangePage()
    {
        return this.PageLoad;
    }
}