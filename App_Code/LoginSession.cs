﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data.OleDb;

/// <summary>
/// LoginSession 的摘要描述
/// </summary>
/// 
public class LoginSession
{
    private bool _IsLogin = false;
    private string _UserName = string.Empty;
    private string _Password = string.Empty;
    private string _Status = string.Empty;
    private int _TimeOut = 10;
    private int _Power = 1;
    private string _Display = string.Empty;      // 館流水號
    private string _DisplayNum = string.Empty;   // 館代號(API)
    private string _DisplayTitle = string.Empty; // 館名
    private bool _HasIEK = false;
    private string _CustomNum = string.Empty;

    public OleDbConnection db;
    public OleDbCommand cmd;

    public string UserName
    {
        get { return this._UserName; }
    }

    public bool IsLogin
    {
        get { return this._IsLogin; }
    }

    public string Password
    {
        get { return this._Password; }
    }

    public string Status
    {
        get { return this._Status; }
    }

    public int TimeOut
    {
        get { return this._TimeOut; }
    }

    public int Power
    {
        get { return this._Power; }
    }

    public string Display
    {
        get { return this._Display; }
    }

    public string DisplayNum
    {
        get { return this._DisplayNum; }
    }

    public string DisplayTitle
    {
        get { return this._DisplayTitle; }
    }

    public bool HasIEK
    {
        get { return this._HasIEK; }
    }

    public string CustomNum
    {
        get { return this._CustomNum; }
    }

    public LoginSession(string usrn, string pwd)
    {
        this._UserName = usrn;
        this._Password = pwd;

        db = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|data.accdb");
        db.Open();

        string sql = "SELECT * FROM UserList WHERE username = @username AND passwd = @passwd";
        cmd = new OleDbCommand(sql, db);
        cmd.Parameters.Clear();
        cmd.Parameters.Add(new OleDbParameter("@username", this._UserName));
        cmd.Parameters.Add(new OleDbParameter("@passwd", this._Password));

        OleDbDataReader reader = cmd.ExecuteReader();

        if (reader.HasRows)
        {
            while (reader.Read())
            {
                this._IsLogin = true;
                this._Status = reader.GetString(3);
                this._TimeOut = Convert.ToInt32(reader.GetValue(4));
                this._Power = Convert.ToInt32(reader.GetValue(5));
                this._Display = reader.GetString(6);

                if (usrn != "admin")
                {
                    this._DisplayNum = GetDisplayNum(this._Display);
                    this._DisplayTitle = GetDisplayTitle(this._Display);
                    this._CustomNum = GetCustomNum(this._Display);
                }
            }
        }
        db.Close();
        db.Dispose();
    }


    private string GetDisplayNum(string display)
    {
        string result = string.Empty;
        string[] _display = display.Split(',');

        db = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|data.accdb");
        db.Open();

        foreach (string _ds in _display)
        {
            if (_ds != "")
            {
                string sql = "SELECT num,title FROM DisplayList WHERE id = @id";
                cmd = new OleDbCommand(sql, db);
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new OleDbParameter("@id", Convert.ToInt32(_ds)));
                OleDbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {

                    if (reader.GetValue(0).ToString() != "112" && reader.GetString(1).Contains("自訂") == false)
                    {
                        result += reader.GetValue(0).ToString() + ",";
                    }
                    else if (reader.GetValue(0).ToString() == "112")
                    {
                        this._HasIEK = true;
                    }
                }
            }
        }

        db.Close();
        db.Dispose();
        return result.Substring(0, result.Length - 1);
    }





    private string GetDisplayTitle(string display)
    {
        string result = string.Empty;
        string[] _display = display.Split(',');

        db = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|data.accdb");
        db.Open();

        foreach (string _ds in _display)
        {
            if (_ds != "")
            {
                string sql = "SELECT title FROM DisplayList WHERE id = @id";
                cmd = new OleDbCommand(sql, db);
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new OleDbParameter("@id", Convert.ToInt32(_ds)));
                OleDbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {

                    if (reader.GetString(0).Contains("IEK") == false && reader.GetString(0).Contains("自訂") == false)
                    {
                        result += reader.GetValue(0).ToString() + ",";
                    }

                }
            }
        }

        db.Close();
        db.Dispose();
        return result.Substring(0, result.Length - 1);
    }


    private string GetCustomNum(string display)
    {
        string result = string.Empty;
        string[] _display = display.Split(',');

        db = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|data.accdb");
        db.Open();

        foreach (string _ds in _display)
        {
            if (_ds != "")
            {
                string sql = "SELECT num,title FROM DisplayList WHERE id = @id";
                cmd = new OleDbCommand(sql, db);
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new OleDbParameter("@id", Convert.ToInt32(_ds)));
                OleDbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    string _title = reader.GetValue(1).ToString();
                    string _num = reader.GetValue(0).ToString();
                    if (_title.Contains("自訂"))
                    {
                        result += _num + ",";
                    }
                }
            }
        }

        db.Close();
        db.Dispose();
        if (result == "")
        {
            return "";
        }
        return result.Substring(0, result.Length - 1);
    }

}