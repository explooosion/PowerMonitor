﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data.OleDb;


public partial class MasterPage : System.Web.UI.MasterPage
{
    public string usr = string.Empty; // user name

    public OleDbConnection db = null;
    public OleDbCommand cmd = null;
    public LoginSession ls;

    protected void Page_Load(object sender, EventArgs e)
    {
        ls = (LoginSession)Session["LoginSession"];
        if (ls != null)
        {
            if (ls.IsLogin == true)
            {
                usr = ls.UserName.ToString();

                if (Session["DisplayNumArray"] == null)
                {

                    // 初始化-畫面停留時間
                    Session.Add("TimeOut", ls.TimeOut);
                    Session.Add("HasIEK", ls.HasIEK);

                    // 初始化-館陣列
                    Session.Add("DisplayNumArray", ls.DisplayNum);
                    Session.Add("DIsplayTitleArray", ls.DisplayTitle);
                    Session.Add("CustomNumArray", ls.CustomNum);

                    // 初始化-當前館-識別資料
                    Session.Add("DisplayNum", ls.DisplayNum.Split(',')[0]);
                    Session.Add("DisplayTitle", ls.DisplayTitle.Split(',')[0]);
                }
            }
            else
            {
                LogOut();
            }
        }
        else
        {
            LogOut();
        }
    }

    private void LogOut()
    {
        Session.Clear();
        Session.Abandon();
        Response.Redirect("Login.aspx");
    }
}
