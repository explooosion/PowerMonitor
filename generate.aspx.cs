﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class generate : System.Web.UI.Page
{
    LoginSession ls;
    public string view_name = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        ls = (LoginSession)Session["LoginSession"];

        if (IsPostBack == true)
        {
            Response.Redirect("powerChannel.aspx");
        }
    }
}
