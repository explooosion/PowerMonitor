﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Bulletin.aspx.cs" Inherits="Bulletin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <style>
        html, body {
            margin: 0;
            padding: 0;
            overflow: hidden;
        }
    </style>
    <script src="js/jquery-1.11.3.min.js"></script>
    <script>


        let CustomNumArrayTmp = '<%=Session["CustomNumArray"]%>'.split(',');
        let CustomNumArray = [];
        for (let index in CustomNumArrayTmp) {
            CustomNumArray.push(Number(CustomNumArrayTmp[index]));
        }
        console.log('custom - ' + CustomNumArray);

        let PageName = CustomNumArray[0];



        $(window).ready(function () {

            let h = $(window).height();
            let w = $(window).width();
            $("#ifmBulletin").height(h).width(w).attr('src', GetPageUrl(PageName)).load();

            setTimeout(ChangePage, 3000);

        });

        function ChangePage() {

            if (CustomNumArray.indexOf(PageName) == CustomNumArray.length - 1) {

                let hasiek = '<%=Session["HasIEK"]%>';
                console.log(hasiek);

                if (hasiek == 'True') {
                    window.location.href = "iek.aspx";
                } else {
                    window.location.href = "powerChannel.aspx";
                }

            } else {

                let index = CustomNumArray.indexOf(PageName) + 1;
                PageName = CustomNumArray[index];
                $("#ifmBulletin").attr('src', GetPageUrl(PageName));

                setTimeout(ChangePage, 3000);

            }
        }

        function GetPageUrl(PageName) {
            return 'custom/bulletin' + PageName + '.html';
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <iframe id="ifmBulletin" src="custom/bulletin1.html" frameborder="0"></iframe>
        </div>
    </form>
</body>
</html>
