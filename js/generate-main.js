var mainscript = {

    genPower: function () {

        // generate1.json

        let pp = new PowerPerriod();

        pp.instant = re.PowerW;

        let date = new Date();
        let nYear = date.getFullYear().toString(); // 今年度年份
        let lYear = (date.getFullYear() - 1).toString(); // 去年度年份
        let nMonth = (date.getMonth()).toString(); // 截止至上個月

        for (let i in re.HistoryEnergy) {
            switch (i.substring(0, 4)) {
                case nYear:

                    // 今年累計用電 A (累加資料 = 1月 ~ 本月)
                    pp.nowUse = Math.round(re.HistoryEnergy[i].AccumulativeUsedEnergy);
                    break;
                case lYear:

                    // 去年累計用電 B (累加資料 = 1月 ~ 本月)
                    if (nMonth >= Number(i.substring(5, 7))) {
                        pp.lastUse = Math.round(re.HistoryEnergy[i].AccumulativeUsedEnergy);
                    }

                    // 去年度總用電量 - 電量
                    pp.lastUseAll = Math.round(re.HistoryEnergy[i].AccumulativeUsedEnergy);
            }
        }

        pp.nowPeriod = nYear + ".1 ~ " + nYear + "." + nMonth;
        pp.lastPeriod = lYear + ".1 ~ " + lYear + "." + nMonth;
        pp.accumuUse = Math.round((pp.lastUse - pp.nowUse) / pp.lastUse * 100);
        pp.saveAmount = pp.lastUse - pp.nowUse;
        pp.lastYear = lYear; // 去年度總用電量 - 年份
        pp.savePercent = Math.abs(pp.accumuUse);
        pp.usePercent = 100 - pp.savePercent;

        $("#instantPowerVal").text(pp.instant);
        $("#powerValA").text(pp.nowUse);
        $("#periodA").text(pp.nowPeriod);
        $("#powerValB").text(pp.lastUse);
        $("#periodB").text(pp.lastPeriod);
        $("#generatePercent").text(pp.accumuUse);
        $("#genPowerAmount").text(pp.saveAmount);
        $("#lastTotal").text(pp.lastUseAll);
        $("#lastYear").text(pp.lastYear);

        // chart
        powChartOne.hideLoading();
        powChartOne.setOption({
            color: ['#33D5C5', '#FE8081'],
            textStyle: {
                color: '#FFF',
            },
            title: {
                text: '發電率',
                textStyle: {
                    color: '#FFF',
                    fontSize: 20,
                },
                subtextStyle: {
                    color: '#95ACC6',
                    fontSize: 12,
                },
                left: 'center',
                top: 10,
                textBaseline: 'middle',
            },
            tooltip: {
                trigger: 'item',
                selectedMode: false,
                formatter: "{a} <br/>{b} : {d}%"
            },
            legend: {
                x: 'center',
                y: 'bottom',
                textStyle: {
                    color: '#95ACC6',
                    fontSize: 12,
                },
                data: ['發電率', '用電率']

            },
            calculable: true,
            series: [
              {
                  name: '發電率',
                  type: 'pie',
                  radius: [20, 60],
                  center: ['50%', '50%'],
                  roseType: 'radius',
                  label: {
                      normal: {
                          textStyle: {
                              color: '#FFF'
                          }
                      }
                  },
                  lableLine: {
                      normal: {
                          textStyle: {
                              color: '#FFF'
                          },
                          smooth: 0.2,
                          length: 10,
                          length2: 20
                      }
                  },
                  data: [
                      { value: pp.savePercent, name: '發電率' },
                      { value: pp.usePercent, name: '用電率' }
                  ]
              }
            ]

        });

    },


    genPowerYear: function () {


        // generate-year.json

        let py = new PowerYear();
        let year = new Array() // 所有年度
        let amount = new Array() // 用電資料
        let amount_key = new Array();

        let percent = new Array(); // 節電率

        for (let r in re.HistoryEnergy) {

            let key_year = r.substring(0, 4);
            if (year.indexOf(key_year) == -1) {
                year.push(key_year);
            }
            // 以年作為 key 放入陣列儲存
            amount_key[key_year] = Math.round(re.HistoryEnergy[r].AccumulativeUsedEnergy);
        }

        for (let y in amount_key) {
            amount.push(amount_key[y]);
        }

        // 節電率
        percent = SavePercent(amount);

        py.year = year;
        py.amount = amount;
        py.percent = percent;


        ikeChartOne.hideLoading();
        // ikeChartOne.setOption(option);
        ikeChartOne.setOption({
            color: ['#33D5C5', '#FFC65B', '#FE8081', '#3FA9F5'],
            textStyle: {
                color: '#95ACC6',
            },
            title: {
                text: '年度發電量趨勢圖',
                textStyle: {
                    color: '#FFF',
                    fontSize: 20,
                },
                subtextStyle: {
                    color: '#95ACC6',
                    fontSize: 12,
                },
                left: 'center',
                top: '6%',
            },
            tooltip: {
                trigger: 'axis'
            },
            grid: {
                top: '20%',
                left: '6%',
                right: '6%',
                bottom: '20%',
                containLabel: true
            },
            legend: {
                name: '發電量',
                textStyle: {
                    color: '#FFF',
                    fontSize: 12,
                    icon: 'circle',
                },
            },
            xAxis: {
                type: 'category',
                name: '日',
                boundaryGap: true,
                axisLine: {
                    lineStyle: {
                        color: '#667A89',
                        width: 2,
                    },
                },
                splitLine: {
                    lineStyle: {
                        color: '#667A89'
                    },
                },
                data: py.year
            },
            yAxis: {
                type: 'value',
                name: '發電量 kWh',
                axisLine: {
                    lineStyle: {
                        color: '#667A89',
                    },
                },
                splitLine: {
                    lineStyle: {
                        color: '#667A89'
                    },
                },
            },
            series: [{
                name: '發電量',
                type: 'line',
                xAxisIndex: 0,
                yAxisIndex: 0,
                lineStyle: {
                    normal: {
                        color: '#33D5C5',
                        width: 2,
                    }
                },
                data: py.amount,
                symbol: 'emptyCircle',
                symbolSize: 8,
                itemStyle: {
                    normal: {
                        borderColor: '#33D5C5',
                        orderWidth: "2",
                    }
                },
            }]
        });

        // 不同年度累計發電率 %
        var ycon = py.percent.length;
        for (var i = 0; i < ycon; i++) {
            $("#generate-yearly").append('<li><span class="value">' + py.percent[i] + '</span> <span class="unit">%</span></li>');
        };
        $("#generate-yearly li").css({ "width": 100 / ycon + "%" });

    },

    generateChart: function () {


        // generate2.json

        let pm = new PowerMonth();
        let UseNowYear = new Array();
        let UseLastYear = new Array();
        let GenerateNowYear = new Array();
        let GenerateLastYear = new Array();
        let Percent = new Array();
        let nowYear = new Date().getFullYear();
        let lastYear = nowYear - 1;

        let legend = new Array(); // 標籤名稱
        legend.push(lastYear + '發電量');
        legend.push(nowYear + '發電量');
        legend.push(lastYear + '用電量');
        legend.push(nowYear + '用電量');

        for (let r in re.HistoryEnergy) {
            let key_year = Number(r.substring(0, 4));
            switch (key_year) {
                case nowYear:
                    UseNowYear.push(Math.round(re.HistoryEnergy[r].MonthUsedEnergy));
                    GenerateNowYear.push(Math.round(re.HistoryEnergy[r].MonthUsedEnergy));
                    break;
                case lastYear:
                    UseLastYear.push(Math.round(re.HistoryEnergy[r].MonthUsedEnergy));
                    GenerateLastYear.push(Math.round(re.HistoryEnergy[r].MonthUsedEnergy));
                    break;
            }
        }

        pm.electricityA = UseNowYear;
        pm.electricityB = UseLastYear;
        pm.generationA = GenerateNowYear;
        pm.generationB = GenerateLastYear;
        pm.percent = AccumulativeUsedPercent(GenerateNowYear, GenerateLastYear);

        objArray = new Array();

        // 發電
        for (let i = 0 ; i < 4 ; i++) {

            obj = new Object();
            switch (i) {
                case 0:
                    obj.name = lastYear + '發電量';
                    obj.data = pm.electricityA;
                    obj.type = 'bar';
                    break;
                case 1:
                    obj.name = nowYear + '發電量';
                    obj.data = pm.electricityB;
                    obj.type = 'bar';
                    break;
                case 2:
                    obj.name = lastYear + '用電量';
                    obj.data = pm.generationA;
                    obj.type = 'line';
                    break;
                case 3:
                    obj.name = nowYear + '用電量';
                    obj.data = pm.generationB;
                    obj.type = 'line';
                    break;
            }
            objArray.push(obj);
        }

        genChartTwo.hideLoading();
        genChartTwo.setOption({
            color: ['#33D5C5', '#FFC65B', '#FE8081', '#3FA9F5'],
            textStyle: {
                color: '#95ACC6',
            },
            title: {
                text: '本年度累積發電趨勢',
                textStyle: {
                    color: '#FFF',
                    fontSize: 20,
                },
                subtextStyle: {
                    color: '#95ACC6',
                    fontSize: 12,
                },
                left: '2%',
                top: '10',
                textBaseline: 'middle',
            },
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'shadow'
                }
            },
            legend: {
                data: legend,
                top: 30,
                textStyle: {
                    color: '#FFF',
                    fontSize: 12,
                    icon: 'circle',
                },
                left: '2%'
            },
            grid: {
                top: '20%',
                left: '2%',
                right: '2%',
                bottom: '20%',
                containLabel: true
            },
            xAxis: {
                type: 'category',
                data: pm.month,
                axisLine: {
                    lineStyle: {
                        color: '#667A89',
                    },
                },
                splitLine: {
                    lineStyle: {
                        color: '#667A89'
                    },
                },
            },
            yAxis: [
              {
                  type: 'value',
                  name: '發電量 kWh',
                  boundaryGap: [0, 0.1],
                  axisLine: {
                      lineStyle: {
                          color: '#667A89',
                      },
                  },
                  splitLine: {
                      lineStyle: {
                          color: '#667A89'
                      },
                  }
              }, {
                  type: 'value',
                  name: '用電量 kWh',
                  axisLine: {
                      lineStyle: {
                          color: '#667A89',
                      },
                  },
                  splitLine: {
                      lineStyle: {
                          color: '#667A89'
                      },
                  }
              }
            ],
            series: objArray
        });

        // 整年度累計發電率 %
        var i = 0;
        $("#generate-month .value").each(function () {
            if (i < pm.percent.length) {
                $(this).append(pm.percent[i]);
                i++;
            } else {
                return false;
            }
        });

    }
};


var chartDom = document.getElementById("powerChartOne");
var powChartOne = echarts.init(chartDom);
powChartOne.showLoading();


var chartDom = document.getElementById("genPowerYear");
var ikeChartOne = echarts.init(chartDom);
ikeChartOne.showLoading();


var dom = document.getElementById("generateChart");
var genChartTwo = echarts.init(dom);
genChartTwo.showLoading();


$(window).load(function () {

   // LoadData(page);
   // console.log('預載資料...');

});