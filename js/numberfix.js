﻿function NumberFix(num) {
    num = num.toFixed(1); // 取到小數第一位
    return num.toString().replace('.0', '').replace(/\B(?=(\d{3})+(?!\d))/g, ","); // 三位一撇
}

function NumberFixInt(num) {
    num = num.toFixed(0); // 取到整數位
    return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","); // 三位一撇
}