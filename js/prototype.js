﻿
// 戶外狀態 (powerChannel、generate)
class OutDoorStatus {
    constructor() {
        this.temperature = null; // 溫度(°C)
        this.humidity = null; // 光照(%RH)
        this.illuminance = null; // 濕度(W/m2)
    }
}


// 六宮格基本訊息 (iek)
class PowerBasic {
    constructor() {
        this.amount = [];
        this.progress = [];
    }
}


// 今年度上月每日累計發電量 (iek)
class PowerLastMonth {
    constructor() {
        this.date = [];
        this.amount = [];
    }
}


// 歷史累計發電量分析 (iek)
class PowerHistoryYear {
    constructor() {
        this.year = [];
        this.amount = [];
    }
}


// 今年與去年之個月累計發電量分析 (iek)
class PowerMonthAnalysis {
    constructor() {
        this.month = ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"];
        this.nowData = [];
        this.lastData = [];
        this.yearLabel = [];
    }
}


// 即時用電 & 累積用電 (powerChannel、generate)
class PowerPerriod {
    constructor() {
        this.instant = null;
        this.nowUse = null;
        this.nowPeriod = null;
        this.lastUse = null;
        this.lastPeriod = null;
        this.saveAmount = null;
        this.accumuUse = null;
        this.lastYear = null;
        this.lastUseAll = null;
        this.savePercent = null; // 節電率
        this.usePercent = null; // 用電率
    }
}


// 年度用電趨勢圖 (powerChannel、generate)
class PowerYear {
    constructor() {
        this.year = null;
        this.amount = null;
        this.percent = null;
    }
}


// 每月累積發/用電趨勢圖 (powerChannel、generate)
class PowerMonth {
    constructor() {
        this.month = ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"];
        this.electricityA = null;
        this.electricityB = null;
        this.generationA = null;
        this.generationB = null;
        this.percent = null;
        this.name = null;
    }
}


// 節電率 (powerChannel、generate)
function SavePercent(data) {

    let result = new Array();
    for (i in data) {
        if (i > 0) {
            // 節電率 = ( 當年資料 - 基準資料 ) / 基準資料 * 100% 
            result.push(Math.round((data[i] - data[0]) / data[i] * 100));
        }
    }
    return result;
}


// 累積發電率 (powerChannel、generate)
function AccumulativeUsedPercent(now, last) {

    let result = new Array();
    for (i in now) {
        result.push(Math.round((now[i] - last[0]) / last[i] * 100, 2));
    }
    return result;
}

