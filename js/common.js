﻿$(function () {

    $("#dialogEdit").dialog({
        title: "帳號權限設定",
        draggable: true,
        autoOpen: false,
        width: 320,
        height: 650,
        position: { my: "top - 100", at: "right", of: window },
    });

    $("#dialogDelete").dialog({
        title: "帳號權限設定",
        draggable: true,
        autoOpen: false,
        width: 250,
        height: 180,
        position: { my: "top - 300", at: "right", of: window },
    });
 
});

function _addUser() {
    $("#dialogEdit").dialog("open").parent().appendTo($("form:first")).css('display', 'inline');
    $("#ddlUserName").css('display', 'none');
    $("#txtUserName").css('display', 'inline').text('');
    return false;
}

function _editUser() {
    $("#dialogEdit").dialog("open").parent().appendTo($("form:first")).css('display', 'inline');
    $("#ddlUserName").css('display', 'inline');
    $("#txtUserName").css('display', 'none').text('');
    return false;
}

function _deleteUser() {
    $("#dialogDelete").dialog("open").parent().appendTo($("form:first")).css('display', 'inline');
    return false;
}

function _closeEdit() {
    $("#dialogEdit").dialog("close");
    $("#dialogDelete").dialog("close");
    return false;
}


 