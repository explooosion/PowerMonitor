﻿function APICon() {

    try {
        re = apiId == 112 ? JSON.parse(GetCookie('iek')) : JSON.parse(GetCookie('main'));

        LoadData(page);
        if (re == 112)
        {
            SaveCookie(re);
        }
        console.log('api - get from cookies.');
        // 預先載入下一個館資料
        if (page == 'powerChannel') {

            LoadNextData();
        }

    } catch (ex) {

        console.log(ex);
        console.log('api - get from server.');

        $.getJSON(api).done(function (data) {

            re = JSON.parse(data)[apiId];
            LoadData(page);
            //SaveCookie(JSON.parse(data)[apiId]);
            // 預先載入下一個館資料
            if (page == 'powerChannel') {

                LoadNextData();
            }
        }).fail(function () {
            console.log('api - get faild');
        });
    }




}


function LoadData(page) {

    switch (page) {
        case 'iek':
            mainscript.ikeChartFirst();
            mainscript.ikeChartSecond();
            mainscript.ikeChartThird();
            mainscript.ikeBarData();
            break;

        case 'powerChannel':
            mainscript.powerChartFirst();
            mainscript.PowerYear();
            mainscript.powerChartSecond();
            SetTitleInfo();
            break;
    }

}


function LoadNextData() {

    let index = DisplayNumArray.indexOf(apiId.toString());
    let nextAPIid;


    // 確認下一筆id,若為最後一筆則取第一個id
    if (index == DisplayNumArray.length - 1) {
        nextAPIid = DisplayNumArray[0];
    } else {
        nextAPIid = DisplayNumArray[index + 1];
    }

    // Main
    //if (nextAPIid == 2) { nextAPIid = 1; }
    let nexAPI = apiUrl + nextAPIid;
    console.log('api - 遇載 ' + nexAPI);

    DelCookie('main');
    $.getJSON(nexAPI).done(function (data) {
        SaveCookie(JSON.parse(data)[nextAPIid]);
    }).fail(function () {

    });


    // IEK
    if (index == DisplayNumArray.length - 1) {

        DelCookie('iek');

        nexAPI = apiUrl + '112';
        $.getJSON(nexAPI).done(function (data) {
            SaveCookie(JSON.parse(data)[112], true);
        }).fail(function () {
            console.log('api error - 112');
        });
    }


}


function SaveCookie(coo, iek) {

    console.log('api - cookie save ↓');
    if (iek == true) {
        document.cookie = 'iek=' + JSON.stringify(coo);
        console.log(JSON.parse(GetCookie('iek')));
    } else {
        document.cookie = 'main=' + JSON.stringify(coo);
        console.log(JSON.parse(GetCookie('main')));

    }

}


function GetCookie(name) {
    var arr = document.cookie.match(new RegExp("(^| )" + name + "=([^;]*)(;|$)"));
    if (arr != null) return unescape(arr[2]); return null;
}


function DelCookie(name) {
    var exp = new Date();
    exp.setTime(exp.getTime() - 1);
    var cval = GetCookie(name);
    if (cval != null) document.cookie = name + "=" + cval + ";expires=" + exp.toGMTString();
}

function DeleteAllCookies() {
    document.cookie.split(";").forEach(function (c) { document.cookie = c.replace(/^ +/, "").replace(/=.*/, "=;expires=" + new Date().toUTCString() + ";path=/"); });
}