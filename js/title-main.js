﻿function SetTitleInfo() {

    if (apiId == 10001) {
        // 10001 工業技術研究院 不顯示
        $('.out-status').hide();
    } else {

        $('.out-status').show();
        let ods = new OutDoorStatus();
        ods.temperature = Math.round(re.Temperature);
        ods.humidity = Math.round(re.Humidity);
        ods.illuminance = Math.round(re.Illuminance);

        document.getElementById('temperature').textContent = ods.temperature;
        document.getElementById('humidity').textContent = ods.humidity;
        document.getElementById('irradiance').textContent = ods.illuminance;
    }
}