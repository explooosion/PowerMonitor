var mainscript = {

    // 九項進度圖表
    ikeBarData: function () {
        var _barData1 = $("#setAmount"), _barpercent1 = $("#setAmount-percent"),
            _barData2 = $("#instantPower"), _barpercent2 = $("#instantPower-percent"),
            _barData3 = $("#totalPower"), _barpercent3 = $("#totalPower-percent"),
            _barData4 = $("#reduceCo2"), _barpercent4 = $("#reduceCo2-percent"),
            _barData5 = $("#totalHours"), _barpercent5 = $("#totalHours-percent"),
            _barData6 = $("#illuminePower"), _barpercent6 = $("#illuminePower-percent");
        _barData7 = $("#buliding"), _barpercent7 = $("#buliding-percent");
        _barData8 = $("#monthly"), _barpercent8 = $("#monthly-percent");

        // iekbardata.json
        let pb = new PowerBasic();
        pb.amount = [2273, 0, 0, 0, 0, 0, 0, 0];
        pb.progress = ["100%", "0%", "0%", "0%", "0%", "0%", "0%", "0%"];

        _barData1.append(NumberFixInt(pb.amount[0]));
        _barData2.append(NumberFixInt(pb.amount[1]));
        _barData3.append(NumberFixInt(pb.amount[2]));
        _barData4.append(NumberFixInt(pb.amount[3]));
        _barData5.append(NumberFixInt(pb.amount[4]));
        _barData6.append(NumberFixInt(pb.amount[5]));
        _barData7.append(pb.amount[6]);
        _barData8.append(pb.amount[7]);

        _barpercent1.css("width", pb.progress[0]);
        _barpercent2.css("width", pb.progress[1]);
        _barpercent3.css("width", pb.progress[2]);
        _barpercent4.css("width", pb.progress[3]);
        _barpercent5.css("width", pb.progress[4]);
        _barpercent6.css("width", pb.progress[5]);
        _barpercent7.css("width", pb.progress[6]);
        _barpercent8.css("width", pb.progress[7]);

    },


    ikeChartFirst: function () {
        // iek1.json 今年上月每日累計發電量
        let plm = new PowerLastMonth();
        let year = new Date().getFullYear();
        let month = new Date().getMonth();

        for (let i in re.HistoryEnergy) {
            if (i.substring(0, 4) == year && Number(i.substring(5, 7)) == Number(month)) {
                //console.log('取得當月每日資料');
            }
        }

        plm.date = ["10/1", "10/2", "10/3", "10/4", "10/5", "10/6", "10/7", "10/8", "10/9", "10/10", "10/11", "10/12", "10/13", "10/14", "10/15", "10/16", "10/17", "10/18", "10/19", "10/20", "10/21", "10/22", "10/23", "10/24", "10/25", "10/26", "10/27", "10/28", "10/29", "10/30"];
        plm.amount = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];


        let nowYear = new Date().getFullYear();
        let mowLastMonth = new Date().getMonth();
        let ikeTitle = nowYear + '年' + mowLastMonth + '月每日累計發電量';

        ikeChartOne.hideLoading();
        // ikeChartOne.setOption(option);
        ikeChartOne.setOption({
            color: ['#33D5C5', '#FFC65B', '#FE8081', '#3FA9F5'],
            textStyle: {
                color: '#95ACC6',
            },
            title: {
                text: ikeTitle,
                textStyle: {
                    color: '#FFF',
                    fontSize: 20,
                },
                subtextStyle: {
                    color: '#95ACC6',
                    fontSize: 12,
                },
                left: 'center',
            },
            tooltip: {
                trigger: 'axis'
            },
            grid: {
                top: '20%',
                left: '6%',
                right: '6%',
                bottom: '10%',
                containLabel: true
            },
            legend: {
                name: '發電量',
                textStyle: {
                    color: '#FFF',
                    fontSize: 12,
                    icon: 'circle',
                },
            },
            xAxis: {
                type: 'category',
                name: '日',
                boundaryGap: true,
                axisLine: {
                    lineStyle: {
                        color: '#667A89',
                        width: 2,
                    },
                },
                splitLine: {
                    lineStyle: {
                        color: '#667A89'
                    },
                },
                data: plm.date
            },
            yAxis: {
                type: 'value',
                name: '發電量 kWh',
                axisLine: {
                    lineStyle: {
                        color: '#667A89',
                    },
                },
                splitLine: {
                    lineStyle: {
                        color: '#667A89'
                    },
                },
            },
            series: [{
                name: '發電量',
                type: 'line',
                xAxisIndex: 0,
                yAxisIndex: 0,
                lineStyle: {
                    normal: {
                        color: '#33D5C5',
                        width: 2,
                    }
                },
                data: plm.amount,
                symbol: 'emptyCircle',
                symbolSize: 8,
                itemStyle: {
                    normal: {
                        borderColor: '#33D5C5',
                        orderWidth: "2",
                    }
                },
            }]
        });

    },



    ikeChartSecond: function () {

        // iek2.json  今年與去年個月累計發電量分析
        let pma = new PowerMonthAnalysis();

        let nowYear = new Date().getFullYear();
        let lastYear = new Date().getFullYear() - 1;
        pma.yearLabel.push(lastYear);
        pma.yearLabel.push(nowYear);

        for (var i in re.HistoryEnergy) {
            switch (i.substring(0, 4)) {
                case nowYear.toString():
                    // 今年每月資料
                    pma.nowData.push(Math.round(re.HistoryEnergy[i].MonthUsedEnergy));
                    break;
                case lastYear.toString():
                    // 去年每月資料
                    pma.lastData.push(Math.round(re.HistoryEnergy[i].MonthUsedEnergy));
                    break;
            }
        }

        let objList = new Array(); // 放置各年資料
        for (let i = 0 ; i < 2 ; i++) {
            let obj = new Object();
            if (i == 0) {
                obj.name = nowYear;
                obj.type = 'bar';
                obj.data = pma.nowData;
            } else {
                obj.name = lastYear;
                obj.type = 'bar';
                obj.data = pma.lastData;
            }
            objList.push(obj);
        }

        ikeChartTwo.hideLoading();
        ikeChartTwo.setOption({
            color: ['#33D5C5', '#FFC65B', '#FE8081', '#3FA9F5'],
            textStyle: {
                color: '#95ACC6',
            },
            title: {
                text: '各月累計發電量分析',
                textStyle: {
                    color: '#FFF',
                    fontSize: 20,
                },
                subtextStyle: {
                    color: '#95ACC6',
                    fontSize: 12,
                },
                left: 'center',
                top: 10,
                textBaseline: 'middle',

            },
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'shadow'
                }
            },
            legend: {
                data: pma.yearLabel,
                top: 30,
                textStyle: {
                    color: '#FFF',
                    fontSize: 12,
                    icon: 'circle',
                },
            },
            grid: {
                top: '28%',
                left: '6%',
                right: '6%',
                bottom: '10%',
                containLabel: true
            },
            xAxis: {
                type: 'category',
                data: pma.month,
                axisLine: {
                    lineStyle: {
                        color: '#667A89',
                    },
                },
                splitLine: {
                    lineStyle: {
                        color: '#667A89'
                    },
                },
            },
            yAxis: {
                type: 'value',
                name: '發電量 kWh',
                boundaryGap: [0, 0.01],
                axisLine: {
                    lineStyle: {
                        color: '#667A89',
                    },
                },
                splitLine: {
                    lineStyle: {
                        color: '#667A89'
                    },
                },
            },
            series: objList
        });

    },


    ikeChartThird: function () {
        // 歷年累計發電量分析

        let phy = new PowerHistoryYear();
        let amount_tmp = new Array();

        for (i in re.HistoryEnergy) {
            if (phy.year.indexOf(i.substring(0, 4)) == -1) {
                phy.year.push(i.substring(0, 4));
            }
            amount_tmp[i.substring(0, 4)] = Math.round(re.HistoryEnergy[i].AccumulativeUsedEnergy);
        }

        // 將 amount_tmp 重整(reset arr key)
        for (let i in amount_tmp) {
            phy.amount.push(amount_tmp[i]);
        }

        ikeChartThree.hideLoading();
        ikeChartThree.setOption({
            color: ['#FE8081', '#3FA9F5', '#33D5C5', '#FFC65B', ],
            textStyle: {
                color: '#95ACC6',
            },
            title: {
                text: '歷年累計發電量分析',
                textStyle: {
                    color: '#FFF',
                    fontSize: 20,
                },
                subtextStyle: {
                    color: '#95ACC6',
                    fontSize: 12,
                },
                left: 'center',
                top: 10,
                textBaseline: 'middle',

            },
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'shadow'
                }
            },
            grid: {
                top: '28%',
                left: '6%',
                right: '6%',
                bottom: '10%',
                containLabel: true
            },
            xAxis: {
                type: 'category',
                data: phy.year,
                axisLine: {
                    lineStyle: {
                        color: '#667A89',
                    },
                },
                splitLine: {
                    lineStyle: {
                        color: '#667A89'
                    },
                },
            },
            yAxis: {
                type: 'value',
                name: 'kWh',
                boundaryGap: [0, 0.1],
                axisLine: {
                    lineStyle: {
                        color: '#667A89',
                    },
                },
                splitLine: {
                    lineStyle: {
                        color: '#667A89'
                    },
                },
            },
            series: [
                {
                    barWidth: '40%',
                    name: '發電量',
                    type: 'bar',
                    data: phy.amount
                }
            ]
        });

    },

};


var chartDom = document.getElementById("firstChart");
var ikeChartOne = echarts.init(chartDom);
ikeChartOne.showLoading();


var dom = document.getElementById("secondChart");
var ikeChartTwo = echarts.init(dom);
ikeChartTwo.showLoading();


var dom = document.getElementById("thirdChart");
var ikeChartThree = echarts.init(dom);
ikeChartThree.showLoading();


$(window).load(function () {

    // LoadData(page);
    // console.log('預載資料...');

});