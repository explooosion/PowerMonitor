﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="powerChannel.aspx.cs" Inherits="powerChannel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentHead" runat="Server">
      <header class="page-header gradient-primary" id="page-header">
        <div class="container">
          <div class="main-header">
            <!-- Branding Name -->
            <div class="navbar-header">
              <div class="branding channel">
                <h1 class="header-logo powchano"> <span class="iek-icon icon-8"></span><%=Session["DisplayTitle"] %></h1>
              </div> <!--/branding-->
              <div class="out-status">
                <span class="iek-icon icon-9"></span>
                <div class="status-name">戶外狀態</div>
                <div class="out-status-group">
                  <div class="status-value">
                    <span id="temperature" class="values">28</span>
                    <span class="unit">°C</span>
                  </div>
                  <div class="status-value">
                    <span id="humidity" class="values">185</span>
                    <span class="unit">%RH</span>
                  </div>
                  <div class="status-value">
                    <span id="irradiance" class="values">0.791</span>
                    <span class="unit">W/m<i class="super">2</i></span>
                  </div>
                </div>
              </div>

              <!--current Time-->
              <div class="current-date-time has-setting">
                <div class="date-time" id="showDate">
                  <span class="date">2016<small>/</small>09<small>/</small>07</span>
                  <span class="time">Am 08:00:00</span>
                </div>
                <div class="setting-wrap">
                  <a href="javascript: _logout();" class="btn-setting">
                    <span class="iek-icon icon-10"></span>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>
</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="contentMain" runat="Server">

          <div class="content-above">
          <div class="boxshadow">
            <div class="flex-container">
              <div class="flex-column-1">
                <div class="iek-control-panel">
                  <div class="wrapper">
                    <div class="instant-power">
                      <div class="name">即時用電</div>
                      <div class="power-value">
                        <div class="iek-icon icon-7"></div>
                        <div class="amount">
                          <span class="value" id="instantPowerVal"></span>
                          <span class="unit">kW</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="flex-column-2 ">
                <div class="panel-grand-total">
                  <div class="row">
                    <div class="col-xs-5">
                      <div id="powerChartOne" style="height: 220px"></div>
                    </div>
                    <div class="col-xs-7">
                      <div class="power-item powera">
                        <div class="name-title">
                          <div class="period" id="periodA">
                            <!-- 2016.1~2016.4 -->
                          </div>
                          <div class="name">
                            累計用電量Ａ
                          </div>
                        </div>
                        <div class="values">
                          <span class="amount" id="powerValA">
                            <!-- 727,655 -->
                          </span>
                          <span class="unit">kWh</span>
                        </div>
                      </div>
                      <div class="power-item powera">
                        <div class="name-title">
                          <div class="period" id="periodB">
                            <!-- 2015.1~2015.4 -->
                          </div>
                          <div class="name">
                            累計用電量B
                          </div>
                        </div>
                        <div class="values">
                          <span class="amount" id="powerValB">
                            <!-- 821,154 -->
                          </span>
                          <span class="unit">kWh</span>
                        </div>
                      </div>
                      <div class="power-item powera">
                        <div class="name-title">
                          <div class="period">
                            (B-A)/B x 100%
                          </div>
                          <div class="name">
                            目前累計用電率
                          </div>
                        </div>
                        <div class="values">
                          <span class="amount text-danger" id="savePowerPercent">
                            <!-- 11.4 -->
                          </span>
                          <span class="unit">%</span>
                          <span class="save-value">節電 <i class="text-danger" id="savePowerAmount"> <!-- 93,499 --> </i> kWh</span>
                        </div>
                      </div>

                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="flex-container">
          <div class="flex-column-1">
            <div class="power-electricity power-yearly">
              <div id="powerYear" style="height: 100%"></div>
              <div class="chart-other">
                <div class="other-bottom">
                  <ul class="percent-xAxis" id="generate-yearly">
                    <!-- <li><span class="value"></span> <span class="unit">%</span></li> -->
                  </ul>
                  <div class="percent-name">
                    各年度節電率（％）<span class="gray">（以2011年為基準）</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="flex-column-2">
            <div class="power-electricity">
              <div id="powerChartTwo" style="height: 100%"></div>
              <div class="chart-other">
                <div class="other-top">
                  <span class="tit"> <span id="lastYear"><!-- 2015 --></span> 年總用電量</span>
                  <span class="value" id="lastTotal">
                    <!-- 2,711,736 -->
                  </span>
                  <span class="unit">kWh</span>
                </div>
                <div class="other-bottom">
                  <ul class="percent-xAxis" id="comparison-yearly">
                    <li><span class="value"></span> <span class="unit">%</span></li>
                    <li><span class="value"></span> <span class="unit">%</span></li>
                    <li><span class="value"></span> <span class="unit">%</span></li>
                    <li><span class="value"></span> <span class="unit">%</span></li>
                    <li><span class="value"></span> <span class="unit">%</span></li>
                    <li><span class="value"></span> <span class="unit">%</span></li>
                    <li><span class="value"></span> <span class="unit">%</span></li>
                    <li><span class="value"></span> <span class="unit">%</span></li>
                    <li><span class="value"></span> <span class="unit">%</span></li>
                    <li><span class="value"></span> <span class="unit">%</span></li>
                    <li><span class="value"></span> <span class="unit">%</span></li>
                    <li><span class="value"></span> <span class="unit">%</span></li>
                  </ul>
                  <div class="percent-name">
                    2015 與 2016 年度比較
                    <span class="gray">累計用電率（%）</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="contentScript" runat="Server">
    <script src="js/power-main.js"></script>
    <script src="js/showTime.js"></script>
</asp:Content>

