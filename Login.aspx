﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>電力資訊廣播系統 - 登入</title>
    <link rel="stylesheet" type="text/css" href="css/normalize.css" />
    <link rel="stylesheet" type="text/css" href="css/style-login.css" />
    <script src="js/prefixfree.min.js"></script>
    <script src="js/common-login.js"></script>
    <script src="js/conn.js"></script>
    <script>
        DelCookie('iek');
        DelCookie('main');
        console.log('api - clean cookies');
        console.log(GetCookie('user'));
    </script>
</head>
<body>
    <form id="form1" runat="server" defaultfocus="txtUserName">
        <div class="login">
            <h1>電力資訊廣播系統</h1>
            <asp:TextBox runat="server" ID="txtUserName" placeholder="Username"></asp:TextBox>
            <asp:TextBox runat="server" ID="txtPassword" placeholder="Password" TextMode="Password"></asp:TextBox>
            <asp:Button runat="server" ID="btnLogin" CssClass="btn btn-primary btn-block btn-large" Text="登入" OnClick="btnLogin_Click" />
        </div>
    </form>
</body>
</html>
